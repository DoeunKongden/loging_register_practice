import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { loginUser, logOut } from '../redux/authSlice';

function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();

  const loginHandler = () => {
    dispatch(loginUser({ email, password }))
  }

  const logOutHandler = () => {
    dispatch(logOut)
  }



  return (
    <div>
      <h3 className='text-center text-3xl font-bold mt-10'>Login User</h3>
      <div className='flex items-center justify-center mt-10 flex-col  gap-y-5'>
        <label>Email : </label>
        <input
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className='border rounded outline-none'
          type="text"
          placeholder='Email'
        />

        <label>Password : </label>
        <input
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
          placeholder='password'
          className='border rounded outline-none'
        />

        <button
          onClick={loginHandler}
          className='bg-blue-500 w-32 h-10 rounded-xl hover:bg-blue-700 text-white'
        >Log in</button>

        <button
          onClick={logOutHandler}
          className='bg-red-600 w-32 h-10 rounded-xl hover:bg-red-700 text-white'
        >Log out</button>
      </div>
    </div>
  )
}

export default Login