import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { registerUser } from '../redux/authSlice';

function Register() {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const dispatch = useDispatch();

    const registerHandler = () => {
        console.log(email, password)
        dispatch(registerUser({ email, password }))
    }

    return (
        <div>
            <h3 className='text-center text-3xl font-bold mt-10'>Register</h3>
            <div className='flex flex-col justify-center mt-10 items-center gap-y-5'>
                <label>Email : </label>
                <input
                    value={email} onChange={(e) => setEmail(e.target.value)}
                    className='border rounded outline-none'
                    type="text"
                    placeholder='Email'
                />

                <label>Password : </label>
                <input
                    value={password} onChange={(e) => setPassword(e.target.value)}
                    type="password"
                    placeholder='password'
                    className='border rounded outline-none'
                />

                <button onClick={registerHandler} className='bg-blue-500 w-32 h-10 rounded-xl hover:bg-blue-700 text-white'>Register</button>
            </div>

        </div>
    )
}

export default Register