import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";


const initialState = {
    msg: '',
    user: '',
    token: '',
    loading: false,
    error: "",
}


const registerUser = createAsyncThunk('registeruser', async (body) => {
    const res = await fetch("http://localhost:8080/api/v1/user/register", {
        method: "post",
        headers: {
            'Content-Type': "application/json",
        },
        body: JSON.stringify(body)
    })
    return await res.json();
});

const loginUser = createAsyncThunk('loginUser', async (body) => {
    const res = await fetch("http://localhost:8080/api/v1/user/login", {
        method: "post",
        headers: {
            'Content-Type': "application/json",
        },
        body: JSON.stringify(body)
    })
    return await res.json();
})



const authSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        addToken: (state, action) => {
            state.token = localStorage.getItem("token")
        },
        addUser: (state, action) => {
            state.user = localStorage.getItem("user")
        },
        logOut: (state, action) => {
            state.token = null;
            localStorage.clear();
        }
    },
    extraReducers: {
        //Extra Reducer for loging in endpoint 
        [loginUser.pending]: (state, action) => {
            state.loading = true
        },
        [loginUser.fulfilled]: (state, { payload: { error, msg, token, user } }) => {
            state.loading = false;

            if (error) {
                state.error = error
            } else {
                state.msg = msg;
                state.token = token;
                state.user = user;

                localStorage.setItem('msg', msg)
                localStorage.setItem('user', JSON.stringify(user))
                localStorage.setItem('token', token)
            }
        },
        [loginUser.rejected]: (state, action) => {
            state.loading = true
        },


        // Extra Reducer for registering endpoint
        [registerUser.pending]: (state, action) => {
            state.loading = true
        },
        [registerUser.fulfilled]: (state, { payload: { error, msg } }) => {
            state.loading = false;
            if (error) {
                state.error = error
            } else {
                state.msg = msg
            }
        },
        [registerUser.rejected]: (state, action) => {
            state.loading = true
        }
    }
})


export default authSlice.reducer
export { registerUser, loginUser }
export const { addToken, addUser, logOut } = authSlice.actions
